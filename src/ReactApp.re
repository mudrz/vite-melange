[%%raw "import './index.css'"];
// [@module] external styles: Js.t({..}) = "./ReactApp.module.css";
[@mel.module] external styles: {. "box": string} = "./ReactApp.module.css";
Js.log2("styles", styles);
Js.log2("box", styles##box);

module Z = {
  [@react.component]
  let make = () => <div className={styles##box}> {React.string("ZZZ")} </div>;
};

module App = {
  // This sample forces an import of Belt.*, so that CI builds can ensure that
  // Melange has been installed correctly for JS bundlers to be able to find it.
  [@react.component]
  let make = () => <div> {React.string("App")} <Z /> </div>;
};

ReactDOM.querySelector("#root")
->(
    fun
    | Some(root) => ReactDOM.render(<App />, root)
    | None =>
      Js.Console.error(
        "Failed to start React: couldn't find the #root element",
      )
  );
